﻿using Common;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace Service2.Controllers
{
    [ApiController]
    [Route("api/number")]
    public class SecondController : ControllerBase
    {
        private static readonly ActivitySource _activitySource = new(ParametersForLogger.GetServiceName(System.Reflection.Assembly.GetEntryAssembly()));

        private  static volatile bool _flag=false;

        /// <summary>
        /// Get api values
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<string> Get()
        {
            string raw;

            using (var activity = _activitySource.StartActivity("CheckSecondController"))
            {
                activity.SetTag("secondControllerNumber", "2");

                var activityEvent = new ActivityEvent("Some useful information from second service",DateTimeOffset.Now,
                   tags: new ActivityTagsCollection { new("Here should be Key", new  { x=1,y=2,coment="anonymous object, struct like"}) });

                using HttpClient client = new HttpClient();
                raw = await client.GetStringAsync("http://third-service/api/number");      
                
                Thread.Sleep(3000);

                activity.AddEvent(activityEvent);


                if (_flag)
                {

                    using HttpClient client2 = new HttpClient();
                    using HttpResponseMessage response = await client2.GetAsync("https://www.gismeteo.ru/");

                    var activityEvent2 = new ActivityEvent("Additional request had happen", DateTimeOffset.Now,
                                                         tags: new ActivityTagsCollection { new("Here should be Key for note", new { x = 111, y = 222, coment = "anonymous object, struct like", responseStatusCode=response.StatusCode
                                                         }) });
                    activity.AddEvent(activityEvent2);
                }

                _flag = !_flag;

                return $"второй {raw}";
            }
            

        }
    }
}
